# Live-IRC

# Content

The Live OS is a designed Live distribution running Blackbox and IRSSI.
It allows to run a Blackbox desktop with irssi. 
It will give the possibility to use IRSSI out of a memstick.

The memstick will start blackbox + irssi with connection to irc.ircnow.org.

![](desktop.png)


# Download 
Live-IRC-FOSS-Live-OS-image-amd64-irssi-v1.img 

Download with a  web browser or use: 

wget -c "https://gitlab.com/openbsd98324/live-irc/-/raw/main/v1/Live-IRC-FOSS-Live-OS-image-amd64-irssi-v1.img.gz"





# Installation

dd if=Live-IRC-FOSS-Live-OS-image-amd64-irssi-v1.img  of=/dev/sdXXX

or 

zcat Live-IRC-FOSS-Live-OS-image-amd64-irssi-v1.img.gz   > /dev/sdXXXX



# MD5

8fa2d64d8e294999aa9f835f6ffd0794  Live-IRC-FOSS-Live-OS-image-amd64-irssi-v1.img.gz




# Boot

It will run grub to boot. 


# Kiwi Gate

  https://kiwiirc.com/nextclient/irc.ircnow.org 



Have Fun!



